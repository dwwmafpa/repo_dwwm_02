<?php
namespace App\Listener;

use App\Entity\Property;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;
use function dump;

/**
 * Description of ImageCacheSunsriber
 *
 * @author 91GA003
 */
class ImageCacheSubscriber implements EventSubscriber {

    /**
     * @var UploaderHelper
     */
    private $uploaderHelper;

    /**
     * @var CacheManager
     */
    private $cacheManager;

    function __construct(CacheManager $cacheManager, UploaderHelper $uploaderHelper) {
        
        $this->cacheManager = $cacheManager;
        $this->uploaderHelper = $uploaderHelper;
    }

    
    public function getSubscribedEvents(){
        return [
        \Doctrine\ORM\Events::preRemove,
        \Doctrine\ORM\Events::preUpdate
        ];
    }

    public function preRemove(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        dump($entity);
        if ($entity instanceof Property){
            $this->cacheManager->remove($this->uploaderHelper->asset($entity, 'imageFile'));
        } 
    }

    public function preUpdate(PreUpdateEventArgs $args) {
        $entity = $args->getEntity();
        dump($entity);
        if (($entity instanceof Property)){
            if ($entity->getImageFile() instanceof UploadedFile){

                //dump($this->uploaderHelper->asset($entity, 'imageFile'));
                $this->cacheManager->remove($this->uploaderHelper->asset($entity, 'imageFile'));
            }
        }
    }

}
