<?php
namespace App\Notification;

use App\Entity\Contact;
use Symfony\Component\Mailer\MailerInterface;
use Twig\Environment;

/**
 * Description of ContactNotification
 *
 * @author Didier
 */
class ContactNotificationMailer {

    /**
     * @var Environment
     */
    private $renderer;

    /**
     * @var MailerInterface
     */
    private $mailer;

    public function __construct(MailerInterface $mailer, Environment $renderer) {
        $this->mailer = $mailer;
        $this->renderer = $renderer;
    }

    
    public function notify(Contact $contact) {
        
        $message = (new \Symfony\Component\Mime\Email())
                //->setFrom($contact->getEmail())
                ->subject('Agence : ' . $contact->getProperty()->getTitle())
                ->from('noreply@agence.fr')
                ->to('contact@agence.fr')
                ->replyTo($contact->getEmail())
                ->html('<h4>ceci est un essai</h4>');
//                ->html($this->renderer->render('emails/contact.html.twig', [
//                    'contact' => $contact
//                ]), 'text/html');
        $this->mailer->send($message);        
    }
    
}
