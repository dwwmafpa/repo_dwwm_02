<?php
namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\Property;

/**
 * Description of Contact
 *
 * @author Didier
 */
class Contact {
    
    /**
     * @var string|null
     * @Assert\NotBlank()
     * @Assert\Length(min=2, max=100)
     */
    private $firstname;

    /**
     * @var string|null
     * @Assert\NotBlank()
     * @Assert\Length(min=2, max=100)
     */
    private $lastname;

    /**
     * @var string|null
     * @Assert\NotBlank()
     * @Assert\Regex(
     *  pattern="/^[0-9]{10}$/"
     * )
     */
    private $phone;    

    /**
     * @var string|null
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @var string|null
     * @Assert\NotBlank()
     * @Assert\Length(min=10)
     */
    private $message;    
    
    /**
     *
     * @var Property|null
     */
    private $property;

    public function getFirstname(): ?string {
        return $this->firstname;
    }

    public function getLastname(): ?string {
        return $this->lastname;
    }

    public function getPhone(): ?string {
        return $this->phone;
    }

    public function getEmail(): ?string {
        return $this->email;
    }

    public function getMessage(): ?string {
        return $this->message;
    }

    public function setFirstname(?string $firstname) {
        $this->firstname = $firstname;
        return $this;
    }

    public function setLastname(?string $lastname) {
        $this->lastname = $lastname;
        return $this;
    }

    public function setPhone(?string $phone) {
        $this->phone = $phone;
        return $this;
    }

    public function setEmail(?string $email) {
        $this->email = $email;
        return $this;
    }

    public function setMessage(?string $message) {
        $this->message = $message;
        return $this;
    }

    public function getProperty(): ?Property {
        return $this->property;
    }

    public function setProperty(?Property $property) {
        $this->property = $property;
        return $this;
    }

   
}


